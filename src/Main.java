import com.zuitt.example.Contact;
import com.zuitt.example.Phonebook;

public class Main {
    public static void main(String[] args) {
        //System.out.println("Hello world!");

        Phonebook phonebook = new Phonebook();

        Contact contact1 = new Contact("John Doe","+639152468596", "Quezon City");

        Contact contact2 = new Contact("Jane Doe","+639152468596","Caloocan City");

        Contact contact3 = new Contact("Josue Julian","+639504989888","Pasig City");

        phonebook.setContacts(contact1);
        phonebook.setContacts(contact2);
        phonebook.setContacts(contact3);


        if(phonebook.getContacts().size() == 0 ){
            System.out.println("Your phonebook is empty.");
        } else {
            for (Contact contact : phonebook.getContacts()){
                System.out.println(contact.getName());
                System.out.println(contact.getName() + " has the following registered number:");
                System.out.println(contact.getContactNumber());
                System.out.println(contact.getName() + " has the following registered address:");
                System.out.println("my home in " + contact.getAddress());
                System.out.println("_________________________________________________________");
            }
        }
    }
}

